
def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn
from sklearn.model_selection import GridSearchCV
from time import time
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression



start = time()
forest=RandomForestClassifier()
logreg=LogisticRegression()

forest_params = {
    "max_depth": [1,3,10, None],
    "max_features": [1, 3, 5, 10],
    "min_samples_split": [2, 3,5, 10],
    'min_samples_leaf': [2,3, 4, 5,6],
    'n_estimators': [10, 20, 30, 100]
}
logreg_params  = {
    'penalty' : ['l1', 'l2'],
    'C' : np.logspace(-4, 4, 20),
    'solver' : ['liblinear']
}

def get_tuned_random_forest(X_train,Y_train):
    gs = GridSearchCV(forest, forest_params, cv=2, scoring='roc_auc', n_jobs=3)
    gs.fit(X_train, Y_train)
    print("GridSearchCV took %.2f seconds for %d candidate parameter settings."
        % (time() - start, len(gs.cv_results_['params'])))
    print("Random Forest best parameters : ",gs.best_params_)
    print("Random Forest best score : ",gs.best_score_)
    return gs.best_estimator_
def get_tuned_logreg(X_train,Y_train):
    gs = GridSearchCV(logreg, logreg_params, cv=2, scoring='roc_auc', n_jobs=3)
    gs.fit(X_train, Y_train)
    print("GridSearchCV took %.2f seconds for %d candidate parameter settings."
        % (time() - start, len(gs.cv_results_['params'])))
    print("Logistic Regression best parameters : ",gs.best_params_)
    print("Logistic Regression best score : ",gs.best_score_)
    return gs.best_estimator_
