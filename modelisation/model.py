from time import time
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split,cross_val_score
from sklearn.linear_model import LogisticRegression
# import warnings filter
from warnings import simplefilter
# ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)

start = time()

def random_forest_fit(X,Y,random=RandomForestClassifier()):
    xval = cross_val_score(random,X,Y,cv=2,scoring='roc_auc')
    print("Random Forest Accuracy: %0.2f (+/- %0.2f)" % (xval.mean(), xval.std() * 2))
    return random

def logistic_regression_fit(X,Y,logreg=LogisticRegression()):
    # ignore all future warnings
    xval = cross_val_score(logreg,X,Y,cv=2,scoring='roc_auc')
    print("Logistic Regression Accuracy: %0.2f (+/- %0.2f)" % (xval.mean(), xval.std() * 2))
    return logreg
    