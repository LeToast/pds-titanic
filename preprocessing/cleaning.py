import numpy as np

def cleaning(data):
  df = data.apply(lambda x:x.fillna( x.median() if np.issubdtype(x.dtype, np.number) else x.mode().iloc[0] ))
  # remplace les valeurs manquantes par la mediane ou le mode en fonction du type
  return df
