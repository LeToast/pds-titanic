import numpy as np
import seaborn as sns

#affichons nos variables numériques en fonction de la survie
def plot (data):
    data.plot(kind='scatter', x='Age', y='Fare', c='Survived', s=50, cmap='viridis')
    data.get(['Fare', 'Age', 'Survived']).groupby('Survived').mean().plot(kind='bar')
    # On fait le logarithme afin d'obtenir des valeurs plus uniformes
    data["logFare"] = np.log(data.Fare.values + 10.)
    sns.set()
    sns.set_style("whitegrid")
    sns.jointplot(data.Age[data.Survived == 1],
                data.logFare[data.Survived == 1],
                kind="kde", height=7, space=0, color="g")

    sns.jointplot(data.Age[data.Survived == 0],
                data.logFare[data.Survived == 0],
                kind="kde", height=7, space=0, color="r")

def stats (data):
    print(data.count())
    print(list(data.columns))
    print(data.shape)
    print(data.dtypes)
    print(data.groupby('Survived').count())
    print("Survival : ",np.mean(data.get('Survived') == 0)*100,"%")
