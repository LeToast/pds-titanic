import numpy as np

#separation de l'age en bins
def split_age(data):
    temp = pd.cut(x=data['Age'], bins=[0,5, 18, 39, 99])
    data['Age']=temp
    return data
import numpy as np
import pandas as pd
import re as re



# fonction de split de la fonction
def split_title_func(names):
    return re.findall(', (\w+).|$', names)[0]
# récupération du titre de la personne
def split_title(data):
    data['TitleT']=data['Name'].apply(lambda x : split_title_func(x))
    return data

# fonction de split du surname
def split_surname_func(names):
    return re.findall('\([\w\s]+\)|$', names)[0]
# récupération du surnom de la personne
def split_surname(data):
    data['Surname']=data['Name'].apply(lambda x : split_surname_func(x))
    data['Surname?']=data['Surname'].apply(lambda x : x!='')
    return data

def features_pipeline(data):
    data=split_age(data)
    data=split_surname(data)
    dafa=split_title(data)
    # on ne garde que les colonnes les plus pertinentes pour alleger le modèle
    data = data.get(['Fare','Pclass','Age','Sex','Embarked','Surname?','Survived'])
    # on transforme les colonnes qualitatives en booleans
    data = pd.get_dummies(data)
    # on drop le sex_male car redondant avec sex_female
    data=data.drop('Sex_male',axis=1)
    return data