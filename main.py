import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import re as re
import pickle

from preprocessing.profiling import *
from preprocessing.cleaning import *
from pipeline.feature_engineering import *
from modelisation.model import *
from modelisation.tuning import *

data = pd.read_csv('data/train.csv')
# Graphiques
plot(data)

# Statistiques
stats(data)

# Cleaning
data=cleaning(data)
print(data.count())

# Features Extraction
data=features_pipeline(data)
print(data)

# Target, X and Y splitting
Y = data['Survived']
X = data.drop("Survived",axis=1)
X_train, X_test, Y_train, Y_test = train_test_split(X, Y)

# Test des deux modèles
forest=random_forest_fit(X,Y)
logreg=logistic_regression_fit(X,Y)

# Recherche de meilleurs paramètres pour la regression logistique
tunedLogreg=get_tuned_logreg(X_train,Y_train)
# Recherche de meilleurs paramètres pour le random forest
tuned_random_forest=get_tuned_random_forest(X_train,Y_train)

# sauvegarde des modèles
pickle.dump(tuned_random_forest, open('ModelSave/Random_forest.sav', 'wb'))
pickle.dump(tunedLogreg, open('ModelSave/Logistic_regression.sav', 'wb'))